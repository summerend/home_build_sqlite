# FAQ




* sizeof and strlen?

* strncmp(buf1, buf2, num)
bool(buf1[0:num] == buf2)


* vscode multicursor on ubuntu
   
    1. command:  toogle multi-cursor modifer
    2. `ctrl+shift+left-mouse`


* exact parts from string by pattern

use sscanf    

```c
int args_assigned = sscanf(
    input_buffer->buffer, "insert %d %s %s", &(statement->row_to_insert.id),
    statement->row_to_insert.username, statement->row_to_insert.email);
if (args_assigned < 3) {
    return PREPARE_SYNTAX_ERROR;
}
```

* page in os
"The operating system will move pages in and out of memory as whole units instead of breaking them up."